/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/qenv',
  version: '5.0.2',
  description: 'easy promised environments'
}
